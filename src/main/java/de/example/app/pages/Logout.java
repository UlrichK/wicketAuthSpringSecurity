package de.example.app.pages;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.Link;

/**
 * <p>Created by ulrich.knaack on 23.07.2018.</p>
 */
public class Logout extends WebPage {
    public Logout() {
        add(new Link<Void>("login") {
            @Override
            public void onClick() {
                setResponsePage(Login.class);
            }
        });
    }
}
