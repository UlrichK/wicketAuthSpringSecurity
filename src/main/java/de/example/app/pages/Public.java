package de.example.app.pages;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.Link;

public class Public extends WebPage {
    public Public() {
        super();
        add(new Link<Void>("user") {
            @Override
            public void onClick() {
                setResponsePage(UserStart.class);
            }
        });
        add(new Link<Void>("admin") {
            @Override
            public void onClick() {
                setResponsePage(Admin.class);
            }
        });
    }
}
