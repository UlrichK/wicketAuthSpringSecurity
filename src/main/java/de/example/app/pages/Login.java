package de.example.app.pages;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.wicket.Application;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.flow.RedirectToUrlException;
import org.apache.wicket.util.string.Strings;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.savedrequest.SavedRequest;

public class Login extends WebPage {
    private String username;
    private String password;

    @Override
    protected void onInitialize() {
        super.onInitialize();
        StatelessForm form = new StatelessForm<Void>("form") {
            @Override
            protected void onSubmit() {
                super.onSubmit();
                if (Strings.isEmpty(username) || Strings.isEmpty(password)) { return; }

                String originalUrl = getOriginalUrl();

                try {
                    boolean authResult = AuthenticatedWebSession.get().signIn(username, password);

                    if (authResult) {
                        if (originalUrl != null) {
                            throw new RedirectToUrlException(originalUrl);
                        }
                        setResponsePage(Application.get().getHomePage());
                    }
                    else {
                        error("wrong username/password");
                    }
                }
                catch (AuthenticationException e) {
                    error("wrong username/password");
                }
            }
        };
        form.setModel(new CompoundPropertyModel<>(this));
        add(form);

        form.add(new RequiredTextField<>("username"));
        form.add(new PasswordTextField("password"));
        form.add(new FeedbackPanel("feedback"));
    }

    /**
     * This method is based on sources from https://github.com/dmbeer/wicket-7-spring-security
     *
     * Returns the URL the user accessed before he was redirected to the login page. This URL has
     * been stored in the session by spring
     * security.
     *
     * @return the original URL the user accessed or null if no URL has been stored in the session.
     */
    private String getOriginalUrl() {
        HttpServletRequest servletRequest =
                (HttpServletRequest) RequestCycle.get().getRequest().getContainerRequest();
        HttpSession session = servletRequest.getSession();
        SavedRequest savedRequest =
                (SavedRequest) session.getAttribute("SPRING_SECURITY_SAVED_REQUEST");
        if (savedRequest != null) {
            return savedRequest.getRedirectUrl();
        }
        else {
            return null;
        }
    }
}
