package de.example.app.pages;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.Link;

@AuthorizeInstantiation("admin")
public class Admin extends WebPage {
    public Admin() {
        super();
        add(new Link<Void>("user") {
            @Override
            public void onClick() {
                setResponsePage(UserStart.class);
            }
        });
        add(new Link<Void>("public") {
            @Override
            public void onClick() {
                setResponsePage(Public.class);
            }
        });
        add(new Link<Void>("logout") {
            @Override
            public void onClick() {
                AuthenticatedWebSession.get().invalidate();
                setResponsePage(Logout.class);
            }
        });
    }
}
