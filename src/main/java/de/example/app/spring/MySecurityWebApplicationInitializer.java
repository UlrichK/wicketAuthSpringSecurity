package de.example.app.spring;

import java.util.EnumSet;
import javax.servlet.*;

import org.slf4j.*;

import org.apache.wicket.protocol.http.WicketFilter;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

import de.example.app.MyApplication;

public class MySecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer
{
    private static Logger logger =
            LoggerFactory.getLogger(MySecurityWebApplicationInitializer.class);

    public MySecurityWebApplicationInitializer()
    {
        super(MySecurityConfigurerAdapter.class);
    }

    @Override
    protected void beforeSpringSecurityFilterChain(ServletContext servletContext)
    {
        super.beforeSpringSecurityFilterChain(servletContext);
    }

    @Override
    protected void afterSpringSecurityFilterChain(ServletContext servletContext)
    {
        super.afterSpringSecurityFilterChain(servletContext);

        Filter myWicketFilter=new WicketFilter(new MyApplication())
        {
            @Override
            public void init(boolean isServlet, FilterConfig filterConfig) throws ServletException
            {
                setFilterPath("");
                super.init(isServlet, filterConfig);
            }
        };

        FilterRegistration.Dynamic wicketRegistration;
        wicketRegistration = servletContext.addFilter("myWicketFilter", myWicketFilter);
        wicketRegistration.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "*");
    }
}
