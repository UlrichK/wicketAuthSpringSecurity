package de.example.app.spring;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.*;


public class MyUserDetailsService implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Collection<MyAuthority> authorities = new HashSet<>();
        MyAuthority admin = new MyAuthority("admin");
        MyAuthority user = new MyAuthority("user");
        MyAuthority guest = new MyAuthority("guest");

        switch (username) {
            case "tim":
            case "steve":
                authorities.add(admin);
                authorities.add(user);
                authorities.add(guest);
                break;
            case "craig":
                authorities.add(user);
                authorities.add(guest);
                break;
            case "phil":
                authorities.add(guest);
                break;
            default:
                throw new UsernameNotFoundException("user \"" + username + "\" unknown");
        }
        return new User(username, "123456", authorities);
    }

    class MyAuthority implements GrantedAuthority {
        private final String role;

        MyAuthority(String r) {
            role = r;
        }

        @Override
        public String getAuthority() {
            return role;
        }
    }
}
